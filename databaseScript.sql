create table Benutzer
(
    benutzername   varchar(20)                         not null
        primary key,
    name           varchar(50)                         not null,
    password       varchar(100)                        not null,
    eintrittsdatum timestamp default CURRENT_TIMESTAMP not null
);

create table Anzeige
(
    id               int auto_increment
        primary key,
    titel            varchar(100)                        not null,
    text             longtext                            not null,
    preis            decimal(5, 2)                       null,
    erstellungsdatum timestamp default CURRENT_TIMESTAMP not null,
    ersteller        varchar(20)                         not null,
    status           char(8)                             not null,
    kategorie        varchar(45)                         not null,
    constraint Anzeige_Benutzer_benutzername_fk
        foreign key (ersteller) references Benutzer (benutzername)
            on delete cascade
);

create table Kauft
(
    benutzername varchar(20)                         not null,
    anzeigeID    int                                 not null
        primary key,
    kaufdatum    timestamp default CURRENT_TIMESTAMP null,
    constraint Kauft_Anzeige_id_fk
        foreign key (anzeigeID) references Anzeige (id)
            on delete cascade,
    constraint Kauft_Benutzer_benutzername_fk
        foreign key (benutzername) references Benutzer (benutzername)
            on delete cascade
);

create table Kommentar
(
    id               int auto_increment
        primary key,
    text             longtext                            null,
    erstellungsdatum timestamp default CURRENT_TIMESTAMP null,
    username         varchar(20)                         not null,
    anzeigeId        int                                 not null,
    constraint Kommentar_Anzeige_id_fk
        foreign key (anzeigeId) references Anzeige (id)
            on delete cascade,
    constraint Kommentar_Benutzer_benutzername_fk
        foreign key (username) references Benutzer (benutzername)
            on delete cascade
);

create table Nachricht
(
    id         int auto_increment
        primary key,
    text       longtext    null,
    absender   varchar(20) not null,
    empfaenger varchar(20) not null,
    constraint Nachricht_Benutzer_benutzername_fk
        foreign key (absender) references Benutzer (benutzername)
            on delete cascade,
    constraint Nachricht_Benutzer_benutzername_fk_2
        foreign key (empfaenger) references Benutzer (benutzername)
            on delete cascade
);


