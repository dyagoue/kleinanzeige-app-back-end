package impl;

import entities.Benutzer;
import entities.Kommentar;
import utils.KommentarDao;
import utils.StoreException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class KommentarImpl extends BaseService implements KommentarDao {

    public KommentarImpl(){
        super();
    }

    @Override
    public Kommentar addKommentar(int id, String benutzername, String text) {

        Kommentar kommentar = new Kommentar();
        int ins = 0;
        String res = "";

        try {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO Kommentar (text, username, anzeigeId) VALUES (?, ?,?)");
            ps.setString(1, text);
            ps.setString(2, benutzername);
            ps.setInt(3, id);
            ins = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(ins == 1){
            try {
                connection.commit();
            } catch (SQLException e){
                e.printStackTrace();
            }
            kommentar.setText(text);
        } else {
            try {
                connection.rollback();
            } catch (SQLException e){
                e.printStackTrace();
            }
            kommentar = null;

        }
        return kommentar;
    }

    @Override
    public List<Kommentar> getAllKommentar() throws StoreException {

        List<Kommentar> kommentarList = new ArrayList<>();

        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM Kommentar");
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                Kommentar k = new Kommentar();
                k.setId(rs.getInt(1));
                k.setText(rs.getString(2));
                k.setErstellungsdatum(rs.getTimestamp(3));
                k.setUsername(rs.getString(4));
                k.setAnzeigeId(rs.getInt(5));
                kommentarList.add(k);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return kommentarList;
    }

    @Override
    public List<Kommentar> getKommentarByUser(Benutzer benutzer) {
        return null;
    }

    @Override
    public String deleteKommentar(Kommentar kommentar) {
        int del = 0;
        String res = "";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM Kommentar WHERE id=?");
            preparedStatement.setInt(1, kommentar.getId());
            del = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(del == 1){
            try {
                connection.commit();
            } catch (SQLException e){
                e.printStackTrace();
            }
            res = "DELETE OK";
        } else {
            try {
                connection.rollback();
            } catch (SQLException e){
                e.printStackTrace();
            }
            res = "DELETE NOT OK";

        }
        return res;
    }
}
